//fetch('https://www.coolserver.com')
//.then(response => console.log(response))
//.catch(err => console.error(err));

(abc) => 'yeet';

// The promise constructor function takes 1 argument:  a callback function that takes 2 parameters.
// Callback Fn: The first parameter will contain a function to run when the promise is resolved (cb. from the .then(cb))
// Callback fn: The second paramter contains the function to run when the promise is rejected (cb from the .catch(cb))
// A PROMISE IS ASYNC BY NATURE
let superCoolPromise = new Promise((resolve, reject) => {
    //resolve(42);
    resolve('42');
    reject('Array indicies start at 1')
});

superCoolPromise.then(something => console.log(something)).catch(err => console.log(err));

let fetchie = (url) => {
    return new Promise((resolve, reject) => {
        if ( url === 'https://supercoolwebsite.io' ) {
            resolve(['abc', 123]);
        } else {
            let myError = new Error('Ya done goofed!');
            reject(myError);
        }
    })
}

fetchie('https://supercoolwebsite.io').then(response => console.log(response)).catch(err => console.error(err));


console.log('Did this break?');

//let abc = await fetchie ('https://supercoolwebsite.io');

async function letsTalkAboutASYNC () {
    try {
        let response = await fetchie('https://supercoolwebsite.io');
    console.log(response);

    throw new Error('Yeeted out of there');
    
    } catch (e) {
        console.log(e);

    }
}

console.log(letsTalkAboutASYNC());
console.log('yeet');