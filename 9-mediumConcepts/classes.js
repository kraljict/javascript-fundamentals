
//function Person(name, age) {
  //  this.name = name;
    //this.age = age;
    //this.canVote = age >= 18;
    //this.greeting = () => `Hello my name is $( this.name )`;

//}

// Classes are "special functions"
class Person {

    constructor(name, age) {
        this.name = name;
        this.age = age;
        this.canVote = age >= 18;
    }

    greeting() {
        return `Hello my name is ${ this.name }`;
    }



}

//let testing = (abc, bak) => 23 * abc / bak;

//let myResult = `The answer is ${testing(12, 55)} burgers`;
//console.log(myResult);