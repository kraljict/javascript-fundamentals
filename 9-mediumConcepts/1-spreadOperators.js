let array1 = ['a', 'b', 'c'];
let array2 = ['d', 'e',  'f'];

//for (let item of array2) {
    //array1.push(item);
//}

//array1.concat(array2);

// ... tears up the array by element and gives it to the console.
// ... is basically array concatenation
console.log(...['adam']);
console.log('adam');

console.log(...[1,2,3]);
console.log(1,2,3);

console.log(array1);
console.log(...array1);

array1 = [...array1, ...array2 ];

let person1 = {
    name: 'Smokey Bear',
    outfit: 'Bear costume'
}

let bearAttributes = {
    danger: 'RED',
    hunger: 10
}

for (let something in bearAttributes) {
    person1[something] = bearAttributes[something]
}

console.log(person1);

person1 = {...person1, ...bearAttributes};
console.log(person1);