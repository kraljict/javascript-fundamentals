let coffee = ['espresso', 'nitro cold brew', 'americano', 'cappuccino', 'frappuccino'];
console.log(coffee[2]);

let verybest = coffee.pop();
console.log(verybest);

console.log(coffee);

coffee.push('latte');

console.log(coffee);



// Stack, Queue ->
//STACK = Last in, first out 
//Queue = First in, last out
//pop=removes last in array
//push= adds one to the array
//by using .pop and .push, you implemented a stack
// by using .shift and .unshift, you implement a queue
//Array.shift(); removes first element of array
//Array.unshift(); inserts new elements at beginning of array

let firstItem = coffee.shift();
console.log(firstItem);
console.log(coffee);

coffee.unshift('instant', 'drip');
console.log(coffee);

console.log(coffee.indexOf('cappuccino'));

coffee.forEach((item) => {
    console.log(item.length);
});

function forEach (callbackfn) {
    for(let i = 0; i<coffee.length; i++) {
        itemToUse = coffee[i];
        index = i;
        currentArray = coffee;

        callbackfn(itemToUse, index, currentArray);
    }
}

forEach((item, index, myArray) => {
    console.log(item, index, myArray);
})