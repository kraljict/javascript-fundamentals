let unorderedList = document.querySelector('ul');
let submitButton = document.getElementById('submit');
let rocketForm = document.querySelector('form');

const baseURL = 'https://api.spacexdata.com/v2/rockets'

let fetchRockets = () => {
    event.preventDefault();

    fetch(baseURL)
        .then(response => response.json())
        .then( json => {
            console.log(json);
            json.forEach(rocket => {


                // Create the li tag to hold the h1
                // Create an h1 tag to hold the rocket name
                // Attach the name to the h1 tag 
                // Add the h1 tag inside the li tag
                // add the li to the unordered list 

                let li = document.createElement('li');
                let rocketName = document.createElement('h1');

                rocketName.innerText = rocket.name;

                li.appendChild(rocketName);

                unorderedList.appendChild(li);


            })
        })
        .catch( error => console.error(error) );
}

rocketForm.addEventListener('submit', fetchRockets);














// let myData = [
//     { title: 'Downtown is Loud', author: "Adam" },
//     { title: 'My Room is Hot', author: "Nathaniel" },
//     { title: "My Neighbors Are Noisy", author: "Alec" },
//     { title: "Python is Better", author: "Yaboi and Swft" }
// ] ;

// //Goal, loop over myData
// //Loop over the different statements(obj) inside of myData (array)
// for (let thing of myData) {
//     // Create the LI that is going to hold the statement (title, author)
//     let mySuperCoolListItem = document.createElement('li');
//     //Inner text is the objects title

//     // Create the elements that will display the title and author
//     let myTitle = document.createElement('h3');
//     let myAuthor = document.createElement('p');
    
//     // Set the values in the element to display as text (title, author)
//     myTitle.innerText = thing.title; // Sets the TITLE
//     myAuthor.innerText = "- " + thing.author; // Sets the AUTHOR

//     // Add the title and author elements into the list item
//     mySuperCoolListItem.appendChild(myTitle);
//     mySuperCoolListItem.appendChild(myAuthor);

//     //Attach the LI to the unordered list, where it will be read by the user
//     unorderedList.appendChild(mySuperCoolListItem);
// }