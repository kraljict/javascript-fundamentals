let student = { 
    name: "Gloria",
    awesome: true,
    specialty: "Javascript",
    week: 1
};

console.log(student.name);
console.log(student["specialty"])

for (x in student ) {
    console.log(student[x]);
}

console.log(typeof student);

let grades = ["A", "F", "F-", "C++", "B+"];
for (x in grades) {
    console.log(grades[x]);
}

//For-in loop will return to you the KEYS of some collection

