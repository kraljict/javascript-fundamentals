let num = 6;

//Ternary (Condition) ? (true) : (false)

(num > 0) 
  ? console.log(`yes`) 
  : console.log(`nah`);

  //Exactly this 
  if (num > 0) {
      console.log(`yes`);
  } else {
      console.log(`nah`);
  }

  // Challenge, write the AGE IF/ELSE as a big ternary

  let age = 17;

// Older than 25, say "Yay! You can rent a car!"
// Older than 21, but not 25, say "Hey, you can purchase alcohol"
// Older than 18, but not 21, say "Hey, you can vote!"
// Younger than 18, say "You can do stuff one day"

if (age >= 25) {
    console.log('Yay, you can rent a car!');
} else if (age >= 21) {
    console.log('Hey, you can purchase alcohol');
} else if (age >= 18) {
    console.log('Hey, you can vote');
} else {
    console.log('Sorry about it');
}

(age >=25)
    ? console.log(`Yay, you can rent a car!`)
    : (age >= 21)
      ? console.log(`Yay, you can drink!`)
      : (age >= 18)
        ? console.log('Yay, you can vote!')
        : console.log(`Sorry bout it`);

