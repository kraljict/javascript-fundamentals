/*

Make a tip calculator using a function
That takes in the BILL (number)
Have it RETURN the TIP (number)
Capture that returned TIP in a VARIABLE
Print that variable

make a function for tip
paramter = bill (number)
procedure: calculate tip (20%)
return the calculated tip
*/


let tip = (total) =>  {
    return total * 0.2;
}

let tipamount = tip(50);

console.log(tipamount);