let person1 = {
    name: 'Macy',
    age: 24,
    canVote: true
}

let person2 = {
    name: 'Nicolas',
    age: 56,
    canVote: true
}

let person3 = {
    name: 'Argyle',
    age: 15,
    canVote: false
}

//let person1 = createPerson('Macy', 24, true);

//function createPerson (name, age, canVote) {
 //   let person = {
   //     name: name,
     //   age: age,
       // canVote: canVote
    //}

    //return person;
//} 

function Person (name, age) {
    this.name = name;
    this.age = age;
    this.canVote = age >= 18;

    this.greeting = () => `Hello, my name is ${ this.name }`;
    
}

//Instance: A singular occurance of something
//Instances of a Person Object
let macyJones = new Person('Macy', 24);
let sseth = new Person('Seth', 54);
let troy = new Person ('Troy', 12);

console.log(sseth.greeting() )
console.log(troy.greeting() )

//SAME AS BELOW

function Person (name, age) {
    let myObj = {
        name: name,
        age: age,
        canVote: age >= 18,
        greeting = () => `Hello, my name is ${ this.name }`
    }
    return myObj;
}