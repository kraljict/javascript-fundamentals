let returnToSender =  () => {
    let running = () => () => 'yo dawg';
    return running;
}

let value = returnToSender();

console.log(value);

let what = () => returnToSender();
//=> means implicit return
let hmmm = what()()();

console.log(hmmm);

function whatCanIDo() {
    return 6
}